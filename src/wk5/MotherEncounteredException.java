package wk5;

public class MotherEncounteredException extends RuntimeException {
    public MotherEncounteredException(String message) {
        super(message);
    }
}
