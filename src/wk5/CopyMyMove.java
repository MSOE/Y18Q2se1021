package wk5;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import wk5.copyMyMove.Board;
import wk5.copyMyMove.Direction;

import java.io.IOException;

public class CopyMyMove extends Application {
    private int numberOfMoves = 0;
    private int totalMoves = 0;
    private Label movesLabel = new Label("Moves: ");
    private Label levelLabel = new Label("Level: 1");
    private Board level;
    private int levelNumber = 1;

    @Override
    public void start(Stage primaryStage) {
        try {
            // Create a Board object that gets a GridPane and a filename for the levels
            GridPane grid = new GridPane();
            level = new Board(grid, "levels\\level" + levelNumber + ".txt");
            // Have grid pane display above the status bar
            VBox layout = new VBox();
            layout.setOnKeyTyped(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent event) {
                    Direction direction = characterToDirection(event.getCharacter());
                    handlePlayerMove(direction);
                }
            });
            layout.getChildren().addAll(grid, createStatusBar());
            // Add the vertical layout to the scene
            Scene scene = new Scene(layout);
            // Add the scene to the primary stage and show it
            primaryStage.setTitle("Copy My Move");
            primaryStage.setScene(scene);
            layout.requestFocus();
            primaryStage.show();
        } catch (IOException e) {
            Alert noLevel = new Alert(Alert.AlertType.ERROR, "Cannot load level file, exiting");
            noLevel.showAndWait();
        }
    }

    private HBox createStatusBar() {
        // Create a status bar that shows the level number and number of moves spaced horizontally
        HBox statusBar = new HBox();
        statusBar.getChildren().addAll(levelLabel, movesLabel);
        return statusBar;
    }

    private static Direction characterToDirection(String character) {
        Direction direction = Direction.INVALID;
        switch (character) {
            case "h":
            case "H":
            case "a":
            case "A":
                direction = Direction.LEFT;
                break;
            case "j":
            case "J":
            case "s":
            case "S":
                direction = Direction.DOWN;
                break;
            case "k":
            case "K":
            case "w":
            case "W":
                direction = Direction.UP;
                break;
            case "l":
            case "L":
            case "d":
            case "D":
                direction = Direction.RIGHT;
                break;
        }
        return direction;
    }

    private void handlePlayerMove(Direction direction) {
        ++numberOfMoves;
        movesLabel.setText("Moves: " + numberOfMoves);
        if(level.movePlayers(direction)) {
            try {
                totalMoves += numberOfMoves;
                numberOfMoves = 0;
                level.loadLevel("levels\\level" + ++levelNumber + ".txt");
                movesLabel.setText("Moves: " + numberOfMoves);
                levelLabel.setText("Level: " + levelNumber);
            } catch (IOException e) {
                Alert noLevel = new Alert(Alert.AlertType.CONFIRMATION,
                        "You win.  You cleared all " + (levelNumber-1) + " levels in "
                                + totalMoves + " total moves");
                noLevel.showAndWait();
                Platform.exit();
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
