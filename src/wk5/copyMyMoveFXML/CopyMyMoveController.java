package wk5.copyMyMoveFXML;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.layout.GridPane;
import wk5.copyMyMove.Board;
import wk5.copyMyMove.Direction;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CopyMyMoveController implements Initializable {
    @FXML
    private Label movesLabel;
    @FXML
    private Label levelLabel;
    @FXML
    private GridPane gridPane;

    private int numberOfMoves = 0;
    private int totalMoves = 0;
    private Board level;
    private int levelNumber = 1;

    @FXML
    private void handleKeyTyped(KeyEvent keyEvent) {
        Direction direction = characterToDirection(keyEvent.getCharacter());
        handlePlayerMove(direction);
    }

    private static Direction characterToDirection(String character) {
        Direction direction = Direction.INVALID;
        switch (character) {
            case "h":
            case "H":
            case "a":
            case "A":
                direction = Direction.LEFT;
                break;
            case "j":
            case "J":
            case "s":
            case "S":
                direction = Direction.DOWN;
                break;
            case "k":
            case "K":
            case "w":
            case "W":
                direction = Direction.UP;
                break;
            case "l":
            case "L":
            case "d":
            case "D":
                direction = Direction.RIGHT;
                break;
        }
        return direction;
    }

    private void handlePlayerMove(Direction direction) {
        ++numberOfMoves;
        movesLabel.setText("Moves: " + numberOfMoves);
        if(level.movePlayers(direction)) {
            try {
                totalMoves += numberOfMoves;
                numberOfMoves = 0;
                level.loadLevel("levels\\level" + ++levelNumber + ".txt");
                movesLabel.setText("Moves: " + numberOfMoves);
                levelLabel.setText("Level: " + levelNumber);
            } catch (IOException e) {
                Alert noLevel = new Alert(Alert.AlertType.CONFIRMATION,
                        "You win.  You cleared all " + (levelNumber-1) + " levels in "
                                + totalMoves + " total moves");
                noLevel.showAndWait();
                Platform.exit();
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        assert gridPane != null :"fx:id=\"gridPane\" was not injected: check your FXML file 'game.fxml'.";
        try {
            level = new Board(gridPane, "levels\\level" + levelNumber + ".txt");
        } catch (IOException e) {
            Alert noLevel = new Alert(Alert.AlertType.CONFIRMATION,
                    "You win.  You cleared all " + (levelNumber-1) + " levels in "
                            + totalMoves + " total moves");
            noLevel.showAndWait();
            Platform.exit();
        }
    }

    @FXML
    private void handleSwipeUp(SwipeEvent swipeEvent) {
        handlePlayerMove(Direction.UP);
    }
}
