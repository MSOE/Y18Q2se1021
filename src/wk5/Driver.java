package wk5;

import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * Have code that could potential throw an exception
 * Create a try block
 */
public class Driver {

    public static void main(String[] args) {
        try {
            int i = 3/0;
            System.out.println("won't print if exception occurs early in try block");
        } catch(ArithmeticException e) {
            System.out.println("caught the exception");
        } finally {
            System.out.println("will always print");
        }
        System.out.println("prints only if exception was handled");
    }
    private static int promptForInt(String prompt, Scanner in) {
        int result = 0;
        boolean isValid = false;
        while(!isValid) {
            try {
                System.out.println(prompt);
                result = in.nextInt();
                isValid = true;
            } catch (InputMismatchException e) {
                System.out.println(in.next() + " is not a valid integer.");
            }
        }
        return result;
    }

    public static void main3(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println(promptForInt("enter an int", in));
    }

    public static void main2(String[] args) {
        Scanner in = new Scanner(System.in);
        int i = 0;
        try {
            i = in.nextInt();
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        } catch(IndexOutOfBoundsException | IllegalArgumentException e) {
            // do something
            // then rethrow
            throw new RuntimeException();
        } catch(Exception e) {
            in.next();
            i = in.nextInt();
        }
        System.out.println(Math.sqrt(i));
    }
}
