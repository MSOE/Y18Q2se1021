package wk5.copyMyMove;

import javafx.scene.control.Alert;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Board {
    private final GridPane grid;
    private Tile[][] tiles;
    private Player player;
    private Player copyCat;
    private Position finishTile;
    public static final int WIDTH = 450;
    public static final int HEIGHT = 300;
    private static final String PLAYER_AND_COPY = "" + (char)0x222e;

    public Board(GridPane grid, String filename) throws IOException {
        this.grid = grid;
        loadLevel(filename);
        Tile tile = tiles[0][0];
        grid.setMinSize(WIDTH, HEIGHT);
    }

    public GridPane getGridPane() {
        return grid;
    }

    public int getCols() {
        return tiles[0].length;
    }

    public int getRows() {
        return tiles.length;
    }

    private void parseFile(String filename) throws IOException {
        List<String> lines = null;
        try {
            lines = Files.readAllLines(Paths.get(filename));
        } catch (IOException e) {
            System.out.println("Terrible: " + e.getMessage());
            throw new IOException("message sanitize");
        }
        int rows = lines.size();
        int columns = lines.get(0).length();
        int size = Math.min(WIDTH/columns, HEIGHT/rows);
        tiles = new Tile[rows][columns];
        final boolean IS_COPY_CAT = true;
        for(int y = 0; y< tiles.length; ++y) {
            String line = lines.get(y);
            for(int x=0; x<line.length(); ++x) {
                char type = line.charAt(x);
                switch (type) {
                    case '+':
                        player = new Player(x, y);
                        copyCat = new Player(x, y, IS_COPY_CAT);
                        tiles[y][x] = new Tile(' ', size);
                        tiles[y][x].setText(PLAYER_AND_COPY);
                        break;
                    case '|':
                        player = new Player(x, y);
                        tiles[y][x] = new Tile(' ', size);
                        tiles[y][x].visit(player);
                        break;
                    case '-':
                        copyCat = new Player(x, y, IS_COPY_CAT);
                        tiles[y][x] = new Tile(' ', size);
                        tiles[y][x].visit(copyCat);
                        break;
                    case '@':
                        finishTile = new Position(x, y);
                        tiles[y][x] = new Tile(' ', size);
                        tiles[y][x].setBackground(new Background(new BackgroundFill(Color.BLACK, null, null)));
                        break;
                    default:
                        tiles[y][x] = new Tile(type, size);
                }
            }
        }
    }

    public boolean isFinished() {
        return arePlayersOnSameTile() && player.getPosition().equals(finishTile);
    }

    public boolean arePlayersOnSameTile() {
        return player.getPosition().equals(copyCat.getPosition());
    }

    public boolean movePlayers(Direction direction) {
        boolean completedLevel = false;
        int plyRow = player.getPosition().y;
        int plyCol = player.getPosition().x;
        int ccRow = copyCat.getPosition().y;
        int ccCol = copyCat.getPosition().x;
        int numberMoved = 0;
        switch (direction) {
            case LEFT:
                numberMoved = movePlayers(plyRow, plyCol - 1, ccRow, ccCol + 1);
                break;
            case UP:
                numberMoved = movePlayers(plyRow - 1, plyCol, ccRow + 1, ccCol);
                break;
            case RIGHT:
                numberMoved = movePlayers(plyRow, plyCol + 1, ccRow, ccCol - 1);
                break;
            case DOWN:
                numberMoved = movePlayers(plyRow + 1, plyCol, ccRow - 1, ccCol);
                break;
        }
        switch (numberMoved) {
            case 2:
                tiles[ccRow][ccCol].clearPlayer();
            case 1:
                tiles[plyRow][plyCol].clearPlayer();
                plyRow = player.getPosition().y;
                plyCol = player.getPosition().x;
                tiles[plyRow][plyCol].visit(player);
                ccRow = copyCat.getPosition().y;
                ccCol = copyCat.getPosition().x;
                tiles[ccRow][ccCol].visit(copyCat);
                if (arePlayersOnSameTile()) {
                    tiles[plyRow][plyCol].setText(PLAYER_AND_COPY);
                }
                if (isFinished()) {
                    Alert youWin = new Alert(Alert.AlertType.INFORMATION, "Level cleared");
                    completedLevel = true;
                    youWin.showAndWait();
                }
        }
        return completedLevel;
    }

    private int movePlayers(int plyRow, int plyCol, int ccRow, int ccCol) {
        int numberMoved = 0;
        if(plyRow>=0 && plyRow<getRows() && plyCol>=0 && plyCol<getCols() && tiles[plyRow][plyCol].visit(player)) {
            player.setPosition(plyCol, plyRow);
            ++numberMoved;
            if(ccRow>=0 && ccRow<getRows() && ccCol>=0 && ccCol<getCols() && tiles[ccRow][ccCol].visit(copyCat)) {
                copyCat.setPosition(ccCol, ccRow);
                ++numberMoved;
            }
        }
        return numberMoved;
    }

    public void loadLevel(String filename) throws IOException {
        grid.getChildren().clear();
        parseFile(filename);
        for(int y=0; y<getRows(); ++y) {
            for(int x=0; x<getCols(); ++x) {
                grid.add(tiles[y][x], x, y);
            }
        }
    }
}
