package wk5.copyMyMove;

import java.io.Serializable;

public class Player implements Serializable {

    private Position position;
    private final boolean IS_COPY_CAT;
    private static final String PLAYER = "" + (char)0x222b;
    private static final String COPY_CAT = "" + (char)0x2022;
    //private static final String COPY_CAT = "" + (char)0x2299;

    public Player(int x, int y) {
        this(x, y, false);
    }

    public Player(int x, int y, boolean isCopyCat) {
        position = new Position(x, y);
        this.IS_COPY_CAT = isCopyCat;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(int x, int y) {
        this.position = new Position(x, y);
    }

    @Override
    public String toString() {
        return IS_COPY_CAT ? COPY_CAT : PLAYER;
    }
}
