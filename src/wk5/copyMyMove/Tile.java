package wk5.copyMyMove;

import javafx.scene.control.Button;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Tile extends Button {
    private char type;

    public Tile(char type, int size) {
        if(type=='.' || type=='_') {
            type = ' ';
        }
        this.type = type;
        setText("" + type);
        setFont(Font.font("Times Roman", FontWeight.EXTRA_BOLD, size/2.5));
        setDisable(true);
        setPrefSize(size, size);
    }

    public boolean visit(Player player) {
        boolean allowVisitor = false;
        if(type==' ') {
            allowVisitor = true;
            setText(player.toString());
        }
        return allowVisitor;
    }

    public void clearPlayer() {
        setText("" + type);
    }
}
