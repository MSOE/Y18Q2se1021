package wk4;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import wk1.A;

import java.util.Scanner;

/*

 */
public class SillySample extends Application implements EventHandler<ActionEvent> {
    private Label counterLabel;
    private Button clickMe;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Pane root = new VBox();
        clickMe = new Button("Click Me");
        Button dontClickMe = new Button("Don't Click Me");
        clickMe.setOnAction(this);
        dontClickMe.setOnAction(this);
        counterLabel = new Label("Button pressed 0 times");
        root.getChildren().addAll(counterLabel, clickMe, dontClickMe);

        primaryStage.setTitle("Silly Sample");
        primaryStage.setScene(new Scene(root, 400, 300));
        primaryStage.show();
    }

    @Override
    public void handle(ActionEvent event) {
        Scanner parser = new Scanner(counterLabel.getText());
        parser.next();
        parser.next();
        int count = parser.nextInt();
        if(event.getSource()==clickMe) {
            count++;
        } else {
            count--;
        }
        counterLabel.setText("Button pressed " + (count)
               + ((count!=1) ? " times" : " time"));
    }
}
