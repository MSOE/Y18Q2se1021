package wk1;

import javax.swing.*;
import java.util.Scanner;

/* Additional office hours this week:
   * Today: 2pm-5pm
   * Tomorrow: 7am-9am, noon-1pm, 2pm-5pm
   * Monday: 7am-10am, 11am-noon, 2pm-5pm
   *
   * 283814 x 10 ^ 32
 */
public class Driver {
    public static void main(String[] args) {
        String sentence = JOptionPane.showInputDialog(null,
                "Enter a sentence");
        String encrypted = rot13(sentence);
        JOptionPane.showMessageDialog(null, rot13(sentence)
            + "\n\n" + rot13(encrypted));
    }

    private static String rot13(String sentence) {
        String encrypted = "";
        for(int i=0; i<sentence.length(); ++i) {
            char letter = sentence.charAt(++i);
            if((letter>='a' && letter<='m') || (letter>='A' && letter<='M')) {
                encrypted += (char)(letter + 13);
            } else if((letter>='n' && letter<='z') || (letter>='N' && letter<='Z')) {
                encrypted += (char)(letter - 13);
            } else {
                encrypted += letter;
            }
        }
        return encrypted;
    }


    public static void main2(String[] args) {
        System.out.println(Integer.MAX_VALUE);
        System.out.println(Integer.MIN_VALUE);
        /*
        System.out.println("enter a character");
        Scanner in = new Scanner(System.in);
        char letter = in.next().charAt(0);
        int intValue = letter;
        System.out.println(intValue);
        */
    }
}
