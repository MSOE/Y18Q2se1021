package wk6;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.File;

public class FileController {
    @FXML
    public TextField filenameInput;

    @FXML
    public TextArea propertiesOutput;

    @FXML
    public void textChanged(Event event) {
        String fname = filenameInput.getText();
        File test = new File(fname);
        if ( !test.exists() )
            propertiesOutput.setText("...");
        else if ( test.isFile() )
            propertiesOutput.setText("Regular file");
        else if ( test.isDirectory() ) {
            String text = "Directory:\n";
            for(String name : test.list())
                text += "  " + name + "\n";
            propertiesOutput.setText(text);
        } else
            propertiesOutput.setText("other file");
    }
}
