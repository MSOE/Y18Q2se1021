package wk6;

import com.sun.prism.paint.Color;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import wk5.copyMyMove.Board;
import wk5.copyMyMove.Player;
import wk5.copyMyMove.Position;

import java.io.*;
import java.util.Date;

public class Driver {
    public static void main(String[] args) throws IOException {
        writeBinaryObject("out2.bin");
        //readBinary("out.bin");
    }

    private static void writeBinaryObject(String filename) {
        try (FileOutputStream fout = new FileOutputStream(new File(filename));
             ObjectOutputStream dOut = new ObjectOutputStream(fout)) {
            dOut.writeInt(37);
            dOut.writeObject("hellow there ow");
            dOut.writeObject(new Player(0, 0));
            dOut.writeObject(new Position(0, 0));
        } catch (FileNotFoundException e) {
            System.err.println("Could not write file in the given path");
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Could not write to file");
        }
    }

    private static void readBinary(String filename) {
        try (DataInputStream dIn = new DataInputStream(new FileInputStream(new File(filename)))) {
            System.out.println(dIn.readInt());
            try {
                while (true) {
                    System.out.print(dIn.readChar());
                }
            } catch (EOFException e) {
                // done
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeBinary(String filename) {
        try (FileOutputStream fout = new FileOutputStream(new File(filename), true);
             DataOutputStream dOut = new DataOutputStream(fout)) {
            dOut.writeInt(37);
            dOut.writeChars("hellow there ow");
        } catch (FileNotFoundException e) {
            System.err.println("Could not write file in the given path");
        } catch (IOException e) {
            System.err.println("Could not write to file");
        }
    }

    public static void main2(String[] args) {
        DataOutputStream dOut = null;
        try {
            dOut = new DataOutputStream(new FileOutputStream(new File("out.bin")));
            dOut.writeInt(37);
            dOut.writeChars("hellow there ow");
        } catch (FileNotFoundException e) {
            System.err.println("Could not write file in the given path");
        } catch (IOException e) {
            System.err.println("Could not write to file");
        } finally {
            try {
                if(dOut!=null) {
                    dOut.close();
                }
            } catch (IOException e) {
                System.err.println("UGH");
            }
        }
    }
}
