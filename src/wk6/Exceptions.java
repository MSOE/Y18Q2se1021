package wk6;

import java.util.Scanner;

public class Exceptions {
    public static void main(String[] args) {
        try (Scanner in = new Scanner(System.in);
        Scanner in2 = new Scanner("Stuff 4 parsing")) {
            boolean successful = false;
            in2.next();
            in2.next();
            System.out.println(in2.next());
            do {
                try {
                    System.out.println("Enter a word and an integer");
                    String word = in.next();
                    String number = in.next();
                    one(word, number);
                    successful = true;
                } catch (NullPointerException | NumberFormatException e) {
                    System.out.println("Yo, I need an integer for the second value");
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("Please enter a longer word or small integer");
                }
            } while (!successful);
            System.out.println("Have a nice day");
        }
    }

    public static void main2(String[] args) {
        Scanner in = new Scanner(System.in);
        boolean successful = false;
        do {
            try {
                System.out.println("Enter a word and an integer");
                String word = in.next();
                String number = in.next();
                one(word, number);
                successful = true;
            } catch (NullPointerException | NumberFormatException e) {
                System.out.println("Yo, I need an integer for the second value");
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Please enter a longer word or small integer");
            }
        } while(!successful);
        System.out.println("Have a nice day");
        in.close();
    }

    private static void one(String word, String number) {
        int times = Integer.parseInt(number);
        for(int i=0; i<times; ++i) {
            two(word.substring(i));
        }
    }

    private static void two(String substring) {
        System.out.println(substring + " has " + substring.length() + " characters.");
    }
}
