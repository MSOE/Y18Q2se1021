package wk8;

import wk2.Circle;
import wk2.Shape;
import wk2.Square;
import wk2.Triangle;

import java.util.Arrays;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.OptionalDouble;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Functional {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("a", "function", "that", "returns", "a", "function");

        //<editor-fold desc="Counting">
        long count = words.stream()
                          .count();
        long unique = words.stream()
                           .distinct()
                           .count();

        System.out.println("Count: " + count + " Unique: " + unique);
        //</editor-fold>

        //<editor-fold desc="Filtering">
        Predicate<String> isShort = (word -> word!=null && word.length()<5);

        Object[] shortWords = words.stream()
                                   .filter(isShort)
                                   .toArray();

        for(Object word : shortWords) {
            System.out.print(word + " ");
        }
        System.out.println();
        //</editor-fold>

        //<editor-fold desc="Mapping">
        List<String> lengths = words.stream()
                                    .map(word -> word + ": " + word.length())
                                    .collect(Collectors.toList());

        System.out.println(lengths);
        //</editor-fold>

        //<editor-fold desc="Stream Statistics">
        int sum = words.stream()
                       .mapToInt(w -> w.length())
                       .sum();

        System.out.println("Sum of lengths of all strings: " + sum);
        //</editor-fold>

        //<editor-fold desc="ForEach">
        words.stream()
             .forEach(w -> System.out.println(w + ": " + w.length()));

        // Can do forEach directly on a List
        words.forEach(w -> System.out.println(w + ": " + w.length()));
        //</editor-fold>

        //<editor-fold desc="Only Evens">
        Stream<Integer> numbers = Stream.of(3, 8, 7, 2, 1, 9, 8);
        List<Integer> evens = numbers.filter(i -> i%2==0)
                                     .collect(Collectors.toList());

        System.out.println(evens);
        //</editor-fold>

        //<editor-fold desc="Filter by suffix">
        // How many words end with "tion"
        System.out.println(Stream.of("happy", "discussion" /* ... */, "locomotion")
                                 .filter(word -> word.endsWith("tion"))
                                 .count());
        //</editor-fold>

        Stream<Shape> shapes = Stream.of(new Circle(0, 0), new Square(0, 0, 4), new Circle(0, 0),
                new Triangle(0, 0, 1, 5));
        //<editor-fold desc="Extract Circles">
        List<Circle> circles = shapes.filter(shape -> shape instanceof Circle)
                                     .map(shape -> (Circle)shape)
                                     .collect(Collectors.toList());
        //</editor-fold>

        //<editor-fold desc="Area of Shapes">
        shapes = Stream.of(new Circle(0, 0), new Square(0, 0, 4), new Circle(0, 0), new Triangle(0, 0, 1, 5));
        double totalArea = shapes.mapToDouble(shape -> shape.getArea())
                                 .sum();

        System.out.println("Total area: " + totalArea);
        //</editor-fold>

        //<editor-fold desc="Average area of Shapes">
        shapes = Stream.of(new Circle(0, 0), new Square(0, 0, 4), new Circle(0, 0), new Triangle(0, 0, 1, 5));
        OptionalDouble averageArea = shapes.mapToDouble(Shape::getArea)
                                           .average();

        if(averageArea.isPresent()) {
            System.out.println("Average area: " + averageArea.getAsDouble());
        }
        //</editor-fold>

        //<editor-fold desc="Summary Statistics">
        shapes = Stream.of(new Circle(0, 0), new Square(0, 0, 4), new Circle(0, 0), new Triangle(0, 0, 1, 5));
        DoubleSummaryStatistics areaStats = shapes.mapToDouble(Shape::getArea)
                                                  .summaryStatistics();

        System.out.println("Number of shapes: " + areaStats.getCount());
        System.out.println("Total area: " + areaStats.getSum());
        System.out.println("Average area: " + areaStats.getAverage());
        System.out.println("Maximum area: " + areaStats.getMax());
        System.out.println("Minimum area: " + areaStats.getMin());
        //</editor-fold>

        //<editor-fold desc="Reduce">
        shapes = Stream.of(new Circle(0, 0), new Square(0, 0, 4), new Circle(0, 0), new Triangle(0, 0, 1, 5));
        double area = shapes.map(Shape::getArea)
                            .reduce(0.0, (a, b) -> a+b);

        System.out.println("Total area: " + area);
        //</editor-fold>
    }
}
