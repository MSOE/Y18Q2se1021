package wk8;

import javafx.scene.paint.Color;

public interface Transformable {
    Color transform(int x, int y, Color color);
}
