package wk8.twostage;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import wk8.Identity;
import wk8.Transformable;

public class Driver {
    public static void main(String[] args) {
        transformImage(null, new Identity());
        transformImage(null, new Transformable() {
            @Override
            public Color transform(int x, int y, Color color) {
                return color;
            }
        });
        transformImage(null, (x, y, color) -> grayscale(color));
    }

    private static Color grayscale(Color color) {
        return null;
    }

    public static Image transformImage(Image image, Transformable transform) {
        int x = 0;
        int y = 0;
        Color color = null;
        transform.transform(x, y, color);
        return null;
    }
}
