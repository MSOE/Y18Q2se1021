package wk3;

public class Integer extends Calculable {

    private int value;

    public Integer(int value){
        this.value = value;
    }

    @Override
    public Calculable plus(Calculable that){
        if(that instanceof Integer){
            return new Integer((value + that.intValue()));
        } else {
            return that.plus(this);
            //return new Double(value + that.doubleValue());
        }
    }

    @Override
    public Calculable minus(Calculable that){
        if(that instanceof Integer){
            return new Integer((value - that.intValue()));
        } else {
            return new Double(value - that.doubleValue());
        }
    }

    @Override
    public double doubleValue(){
        return value;
    }

    @Override
    public int intValue(){
        return value;
    }
}
