package wk2;

public class Circle extends Shape {
    private double radius;

    public Circle(double x, double y) {
        super(x, y);
        radius = 1.0;
        System.out.println("Circle: constructor");
    }

    @Override
    public void draw() {
        System.out.println("Circle: draw");
    }

    @Override
    public void erase() {
        super.erase();
        System.out.println("Circle: erase");
    }

    @Override
    public void zoom(double magnification) {
        radius *= magnification;
        System.out.println("Circle: zoom");
    }

    @Override
    public double getArea() {
        return Math.PI * Math.PI * radius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
        System.out.println("Circle: setRadius");
    }

    public double getRadius() {
        System.out.println("Circle: getRadius");
        return radius;
    }
}
