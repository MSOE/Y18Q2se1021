package wk2;

public class Triangle extends Shape implements HasArea {

    private double base;
    private double height;

    public Triangle(double x, double y, double base, double height){
        super(x, y);
        this.base = base;
        this.height = height;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public double getArea(){
        return this.base * this.height * 0.5;
    }
    @Override
    public void zoom(double magnification){
        this.height *= magnification;
        this.base *= magnification;
    }

    @Override
    public double getPerimeter() {
        return 0;
    }
}
