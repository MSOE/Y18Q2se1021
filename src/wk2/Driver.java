package wk2;

import javafx.scene.paint.Color;

/*
# Notes:
##
 */
public class Driver {
    public static void main(String[] args) {
        Shape shape1 = new Circle(0, 0);
        if(Math.random()<0.5) {
            shape1 = new Square(9, 9, 2);
        }
        Circle circle1 = new Circle(0, 0);
        if (shape1 instanceof Circle) {
            circle1 = (Circle) shape1;
        }
        System.out.println(shape1.getColor());
        System.out.println(circle1.getColor());
        circle1.setColor(Color.AQUA);
        circle1.setRadius(7);
        System.out.println(circle1);
        circle1.zoom(2);
        System.out.println(circle1.getRadius());
        circle1.erase();
        System.out.println(circle1.getRadius());

    }
}
