package wk2;

public interface HasArea {
    double getArea();
}
