package wk2;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<HasArea> shapes = createShapes(10);
        double area = getAreaOfAllShapes(shapes);

        System.out.println("the are of all your shapes is " + area);

    }

    private static ArrayList<HasArea> createShapes(int numberOfShapes) {
        ArrayList<HasArea> shapes = new ArrayList<HasArea>();
        Scanner in = new Scanner(System.in);
        for(int i = 0; i < numberOfShapes; i++){
            shapes.add(getShape(in));
        }
        return shapes;
    }

    private static double getAreaOfAllShapes(ArrayList<HasArea> shapes) {
        double area = 0;
        for(HasArea shape : shapes) {
            area += shape.getArea();
        }
        return area;
    }

    public static HasArea getShape(Scanner in){
        HasArea shape;
        if(Math.random()>0.5){
            System.out.println("Enter an x coordinate for a triangle");
            double x = in.nextDouble();
            System.out.println("Enter an y coordinate for a triangle");
            double y = in.nextDouble();
            System.out.println("Enter a base for a triangle");
            double base = in.nextDouble();
            System.out.println("Enter a height for a triangle");
            double height = in.nextDouble();
            shape = new Triangle(x,y,base,height);
        } else{
            System.out.println("Enter an x cordanate for a square");
            double x = in.nextDouble();
            System.out.println("Enter an y cordanate for a square");
            double y = in.nextDouble();
            System.out.println("Enter a sideLength for a square");
            double sideLength = in.nextDouble();
            shape = new Square(x,y,sideLength);
        }
        return shape;
    }

}
