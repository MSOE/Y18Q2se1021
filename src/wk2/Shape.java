package wk2;

import javafx.scene.paint.Color;

public abstract class Shape {
    private Color color;
    private double xCoord;
    private double yCoord;

    public Shape(double x, double y) {
        color = Color.WHITE;
        xCoord = x;
        yCoord = y;
        System.out.println("Shape: constructor");
    }

    public void draw() {
        System.out.println("Shape: draw");
    }

    public void erase() {
        System.out.println("Shape: erase");
    }

    public void move() {
        xCoord += 5.0;
        yCoord += 5.0;
        System.out.println("Shape: move");
    }

    public void zoom(double magnification) {
        System.out.println("Shape: zoom");
    }

    public abstract double getPerimeter();

    public abstract double getArea();

    public Color getColor() {
        System.out.println("Shape: getColor");
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
        System.out.println("Shape: setColor");
    }
}
