package wk2;

public class Square extends Shape implements HasArea {
    private double sideLength;

    public Square(double x, double y, double sideLength){
        super(x, y);
        this.sideLength = sideLength;
    }

    public void setSideLength(double sideLength) {
        this.sideLength = sideLength;
    }

    public double getSideLength() {
        return sideLength;
    }

    @Override
    public double getArea(){
        return this.sideLength * this.sideLength;
    }

    @Override
    public void zoom(double magnification){
        this.sideLength *= magnification;
    }

    @Override
    public double getPerimeter() {
        return 0;
    }

}
